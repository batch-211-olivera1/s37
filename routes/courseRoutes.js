const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

//Route for creating a course
// refactor this route to implement user authentication for our admin when creating a course

// router.post("/", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// })

router.post("/",auth.verify,(req,res)=>{

	const data ={
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController=>res.send(resultFromController))
});

router.get("/all",(req,res)=>{
	courseController.retrieveCourses(req.body).then(resultFromController=>res.send(resultFromController));
});

router.get("/",(req,res)=>{
	courseController.retrieveActiveCourses().then(resultFromController=>res.send(resultFromController));
});

router.get("/:courseId",(req,res)=>{
	courseController.retrieveSpecificCourse(req.params).then(resultFromController=>res.send(resultFromController));
});

router.put("/:courseId",auth.verify,(req,res)=>{
	courseController.updateCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});

router.put("/:courseId/archive",auth.verify,(req,res)=>{

	courseController.archiveCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});


module.exports = router;

