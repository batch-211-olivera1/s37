/*
App: Booking System API

	Acourse booking system application where a user can enroll into a course

Type: Course Booking System (Web App)

Description:
	A course booking system application where a user can enroll into a course
	Allows an Admin to do CRUD operation
	Allows users to register into our database

Features:
	-user login(User authentication)
	-User Registration

	Customer/Authenticated Users:
	-View Courses (all active courses)
	-Enroll Course

	Admin Users:
	-Add Course
	-Update Course
	-Archive/Unarchive a course (soft delete/reactivate the course)
	-View Courses (All courses active/inactive)
	-View/Manage User Account**

	All Users(guests, customers, admin)
	-View Active Courses
*/

// Data Model fro the Booking System

// Two-way Embedding

/*
	user{
		
		id - unique identifier for the documents
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin,
		enrollments:[
		{
			id-document identifier
			courseId - unique identifier
			courseName - optional
			isPaid
			dateEnrolled
		}
		]
	}

	course{
		id - unique for the document
		name,
		description,
		price,
		slots,
		isActive,
		createdOn,
		enrollees: [
		{
			id - document identifier
			userId,
			isPaid,
			dateEnrolled

		}

		]

	}

*/


// With Referencing

/*
	user{
		id - unique identifier for the documents
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin,
	}

	course{
		id - unique for the document
		name,
		description,
		price,
		slots,
		isActive,
		createdOn,
	}
	
	enrolment{
		id - unique for the document,
		userId - unique identifier for the user,
		courseId - unique identifier for the course,
		courseName - optional,
		isPaid,
		dateEnrolled
	}
*/

