const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");
// Mini Activity
// Create a new course
/*
	Steps:
	1.Create a new Course object using the mongoose model and the information from the reqBody
		name
		description
		price
	2.Save the new User to the database
	3. Send a screenshot of 3 courses from your database

*/

// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	return newCourse.save().then((course, error) => {

// 		if (error) {

// 			return false;

// 		} else {

// 			return true;

// 		};

// 	});

// };

module.exports.addCourse = (data) => {
	if (data) {
		
	let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});

		return newCourse.save().then((course, error) => {

		if (error) {

			return false;

		} else {

			return true;

		};

	});

	}else {
		return false
	}

};



module.exports.retrieveCourses = () => {
	return Course.find({}).then((result) => {
			return result
		
	})
}


module.exports.retrieveActiveCourses = () =>{
	return Course.find({isActive:true}).then(result=>{
		return result;
	})
};


module.exports.retrieveSpecificCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	})
};


module.exports.updateCourse = (reqParams, reqBody) =>{
let updateCourse = {
	name: reqBody.name,
	description: reqBody.description,
	price: reqBody.price
};
	return Course.findByIdAndUpdate(reqParams.courseId,updateCourse).then((course,error)=>{
		if(error){
			return false;
		}else {
			return true
		}
	});
};


module.exports.archiveCourse = (reqParams, reqBody) =>{
	let archiveCourse = {
		isActive : reqBody.isActive
	}
	
	return Course.findByIdAndUpdate(reqParams.courseId,archiveCourse).then((course,error)=>{
		if(error){
			return false;
		}else {
			return true//course
		}
	});
};
